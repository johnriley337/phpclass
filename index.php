<!doctype html>
<html lang="en" >
    <head>
        <meta charset="utf-8">
        <title>John's Homepage</title>
        <link rel="stylesheet" type="text/css" href="css/base.css">
    </head>
    <body>
        <header><?php include 'includes/header.php' ?></header>
        <nav><?php include 'includes/nav.php' ?></nav>
        <main>
            <img src="images/guyFace.png" alt="Picture Not Found" />
            <p>My name is John Riley. I love programming, creating websites and making electronic projects.</p>
        </main>
        <footer><?php include 'includes/footer.php'?></footer>
    </body>
</html>