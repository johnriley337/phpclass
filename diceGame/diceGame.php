<?php
    //  Variables
    $yourDice1 = mt_rand(1,6);
    $yourDice2 = mt_rand(1,6);
    $compDice1 = mt_rand(1,6);
    $compDice2 = mt_rand(1,6);
    $compDice3 = mt_rand(1,6);
    $yourTotal = $yourDice1 + $yourDice2;
    $compTotal = $compDice1 + $compDice2 + $compDice3;

    if($yourTotal > $compTotal){
        $winner = "You Won!";
    }elseif ($yourTotal == $compTotal) {
        $winner = "It's a Draw";
    } else{
        $winner = "The Computer Beat You.";
    }

    //  Roll image array
    $rollArray = array();
    $rollArray[0] = "";
    $rollArray[1] = "../diceGame/dice_1.png";
    $rollArray[2] = "../diceGame/dice_2.png";
    $rollArray[3] = "../diceGame/dice_3.png";
    $rollArray[4] = "../diceGame/dice_4.png";
    $rollArray[5] = "../diceGame/dice_5.png";
    $rollArray[6] = "../diceGame/dice_6.png";

    //  Assigning images to rolls
    $imgDice1yours = $rollArray[$yourDice1];
    $imgDice2yours = $rollArray[$yourDice2];
    $imgDice1comp = $rollArray[$compDice1];
    $imgDice2comp = $rollArray[$compDice2];
    $imgDice3comp = $rollArray[$compDice3];
?>

<!doctype html>
<html lang="en" >
    <head>
        <meta charset="utf-8">
        <title>John's Dice Game</title>
        <link rel="stylesheet" type="text/css" href="../css/base.css">
    </head>
    <body>
        <header><?php include '../includes/header.php' ?></header>
        <nav><?php include '../includes/nav.php' ?></nav>
        <main>
            <h1><?=$winner?></h1>
            <h2>Your Score:<?=$yourTotal ?></h2>
            <img src="<?php echo $imgDice1yours?>" height="250px"">
            <img src="<?php echo $imgDice2yours?>" height="250px">

            <h2>Computer Score: <?=$compTotal?></h2>
            <img src="<?php echo $imgDice1comp?>" height="250px">
            <img src="<?php echo $imgDice2comp?>" height="250px"">
            <img src="<?php echo $imgDice3comp?>" height="250px">
        </main>
        <footer><?php include '../includes/footer.php'?></footer>
    </body>
</html>
