<?php
/*  This is a countdown timer for the
 *  End Of Semester 07-27-2020
 */
//  Variables
$secPerMin = 60;
$secPerHour = 60 * $secPerMin;
$secPerDay = 24 * $secPerHour;
$secPerYear = 365 * $secPerDay;

//  Current time
$now = time();

//  Last Day Of The Semester
$endSemester = mktime(12,0,0,7,27,2020);

//  Number between now and then
$seconds = $endSemester - $now;

//  Math for formatting
$Years = floor($seconds / $secPerYear);
$seconds = $seconds - ($Years * $secPerYear);

$Days = floor($seconds / $secPerDay);
$seconds = $seconds - ($Days * $secPerDay);

$Hours = floor($seconds / $secPerHour);
$seconds = $seconds - ($Hours * $secPerHour);

$Minutes = floor($seconds / $secPerMin);
$seconds = $seconds - ($Minutes * $secPerMin);


?>

<!doctype html>
<html lang="en" >
    <head>
        <meta charset="utf-8">
        <title>John's End Of Semester Counter</title>
        <link rel="stylesheet" type="text/css" href="../css/base.css">
    </head>
    <body>
        <header><?php include '../includes/header.php' ?></header>
        <nav><?php include '../includes/nav.php' ?></nav>
        <main>
            <h3>End Of Semester Countdown</h3>
            <p>Years: <?=$Years  ?></p>
            <p>Days: <?=$Days ?></p>
            <p>Hours: <?=$Hours  ?></p>
            <p>Minutes: <?=$Minutes  ?></p>
            <p>Seconds: <?=$seconds  ?></p>
        </main>
        <footer><?php include '../includes/footer.php'?></footer>
    </body>
</html>
