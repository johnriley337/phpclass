<?php
    //  Require Slim
    require 'Slim/Slim.php';
    \Slim\Slim::registerAutoloader();

    //  Put = Update
    //  Post = Insert
    //  Get = Select
    //  Delete = Delete

    $app = new \Slim\Slim();

    //  Marathon API
    $app->get('/getRaces', 'get_races');
    $app->get('/getRunners/:race_ID', 'get_runners');
    $app->post('/addRunner', 'add_runner');
    $app->delete('/deleteRunner/', 'delete_runner');

    //  Run
    $app->run();

    //  Marathon API Functions
    function get_races(){
        //  Database Connection
        include '../../includes/dbCon.php';
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("select * from race");
            $sql->execute();
            $results = $sql->fetchAll();
            echo '{"Races":' . json_encode($results) . '}';
            $results = null;
            $db = null;
        }
        catch (PDOException $e){
            $error = $e->getMessage();
            echo json_encode($error);
        }
    }

    function get_runners($race_ID){
        //  Database Connection
        include '../../includes/dbCon.php';
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("select DISTINCT memberLogin.memberName, memberLogin.memberEmail from memberLogin INNER JOIN member_race ON memberLogin.memberID = member_race.memberID WHERE member_race.raceID = :raceID");
            $sql->bindValue(":raceID", $race_ID);
            $sql->execute();
            $results = $sql->fetchAll();
            echo '{"Runners":' . json_encode($results) . '}';
            $results = null;
            $db = null;
        }
        catch (PDOException $e){
            $error = $e->getMessage();
            echo json_encode($error);
        }
    }

    function add_runner(){
        $request = \Slim\Slim::getInstance()->request();
        $post_json = json_decode($request->getBody(), TRUE);
        $memberID = $post_json["memberID"];
        $raceID = $post_json["raceID"];
        $memberKey = $post_json["memberKey"];

        //  Database Connection
        include '../../includes/dbCon.php';
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("select member_race.raceID from member_race INNER JOIN memberLogin ON member_race.memberID = memberLogin.memberID WHERE member_race.raceID = 2 AND memberLogin.memberKey = :apiKey");
            $sql->bindValue(":apiKey", $memberKey);
            $sql->execute();
            $results = $sql->fetch();

            if($results == null){
                echo "Bad API Key";
            } else {
                $sql = $db->prepare("insert into member_race (memberID, raceID, RoleID) VALUES(:memberID, :raceID, 3)");
                $sql->bindValue(":memberID", $memberID);
                $sql->bindValue(":raceID", $raceID);
                $sql->execute();
            }
            $results = null;
            $db = null;
        }
        catch (PDOException $e){
            $error = $e->getMessage();
            echo json_encode($error);
        }
    }

    function delete_runner(){
        $request = \Slim\Slim::getInstance()->request();
        $post_json = json_decode($request->getBody(), TRUE);
        $memberID = $post_json["memberID"];
        $raceID = $post_json["raceID"];

        //  Database Connection
        include '../../includes/dbCon.php';

        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("delete from member_race where memberID = :memberID AND raceID = :raceID");
            $sql->bindValue(":memberID", $memberID);
            $sql->bindValue(":raceID", $raceID);
            $sql->execute();
            $results = null;
            $db = null;
            echo "Deleted runner #$memberID from race $raceID";
        }
        catch (PDOException $e){
            $error = $e->getMessage();
            echo json_encode($error);
        }
    }
?>