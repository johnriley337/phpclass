<?php
session_start();

//  Check login info
if(isset($_POST["txtLoginEmail"])){
    if(isset($_POST["txtLoginPassword"])){

        //  Variables
        $Email = $_POST["txtLoginEmail"];
        $loginPassword = $_POST["txtLoginPassword"];
        $errmsg = "";

        //  Database Connection
        include '../includes/dbCon.php';

        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("select memberPassword, memberKey, RoleID from memberLogin where memberEmail = :Email");
            $sql->bindValue(":Email", $Email);
            $sql->execute();
            $row = $sql->fetch();

            if($row != null) {

                $hashedPassword = md5($loginPassword . $row["memberKey"]);

                if ($hashedPassword == $row["memberPassword"]) {
                    $_SESSION["UID"] = $row["memberID"];
                    $_SESSION["Role"] = $row["RoleID"];
                    if ($row["RoleID"] == 1) {
                        header("Location:admin.php");
                    } else {
                        header("Location:member.php");
                    }
                } else{
                    $errmsg = "Wrong username or password";
                }
            } else{
                $errmsg = "Wrong username or password";
            }
        }
        catch (PDOException $e){
            $error = $e->getMessage();
            $errmsg = "Breakpoint #3";
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3><?=$errmsg?></h3>
    <form method="post">
        <table border="1" width="80%">
            <tr height="60px">
                <td colspan="2"><h3>User Login</h3></td>
            </tr>
            <tr height="40px">
                <th>Email</th>
                <td><input id="txtLoginEmail" name="txtLoginEmail" type="text" size="50"></td>
            </tr>
            <tr height="40px">
                <th>Password</th>
                <td><input id="txtLoginPassword" name="txtLoginPassword" type="password" size="50"></td>
            </tr>
            <tr height="60px">
                <td colspan="2">
                    <input type="submit" value="Login">
                </td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
</html>