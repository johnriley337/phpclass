<?php
session_start();
$errmsg = "";

//  Create Keys
$mKey = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    //  If logged in
    if($_SESSION["Role"] != 1){
        header("Location:index.php");
    }

    //  Checking validity
    if(isset($_POST["submit"])){
        //  Full name
        if(empty($_POST["txtFName"])){
            $errmsg = "Name is required";
        }else{
            $FName = $_POST["txtFName"];
        }
        //  Email
        if(empty($_POST["txtNewMemberEmail"])){
            $errmsg = "Email is required";
        }else{
            $MemberEmail = $_POST["txtNewMemberEmail"];
        }
        //  Password
        if(empty($_POST["txtNewMemberPassword"])){
            $errmsg = "Password is required";
        }else{
            $MemberPassword = $_POST["txtNewMemberPassword"];
        }
        //  Passwords match
        if($MemberPassword != $_POST["txtRetypePassword"]){
            $errmsg = "Passwords do not match";
        }
        //  Role
        if(empty($_POST["txtRole"])){
            $errmsg = "Role is required";
        }else{
            $Role = $_POST["txtRole"];
        }

        //  If passes validation
        if($errmsg == ""){

            //  Database Connection
            include '../includes/dbCon.php';

            //  Do database work
            try {
                $db = new PDO($dsn, $username, $password, $options);
                $sql = $db->prepare("insert into memberLogin (memberName, memberEmail, memberPassword, RoleID, memberKey) VALUE (:fullName, :Email, :Password, :RID, :mKey)");
                $sql->bindValue(":fullName", $FName);
                $sql->bindValue(":Email", $MemberEmail);
                $sql->bindValue(":Password", md5($MemberPassword . $mKey));
                $sql->bindValue(":RID", $Role);
                $sql->bindValue(":mKey", $mKey);
                $sql->execute();

            }
            catch (PDOException $e){
                $error = $e->getMessage();
                echo "Error: $error";
            }

            //  Success
            $FName = "";
            $MemberEmail = "";
            $MemberPassword = "";
            $Role = "";
            $errmsg = "Successfully Added Member";
        }

    }
?>
<!doctype html>
<html lang="en" >
    <head>
        <meta charset="utf-8">
        <title>Admin Page</title>
        <link rel="stylesheet" type="text/css" href="../css/base.css">
    </head>
    <body>
        <header><?php include '../includes/header.php' ?></header>
        <nav><?php include '../includes/nav.php' ?></nav>
        <main>
            <h1>Admin Panel</h1>
            <h3><?=$errmsg?></h3>
            <form method="post">
                <table border="1" width="80%">
                    <tr height="60px">
                        <td colspan="2"><h3>Add New Member</h3></td>
                    </tr>
                    <tr height="40px">
                        <th>Full Name</th>
                        <td><input id="txtFName" name="txtFName" type="text" size="50" value="<?=$FName?>"></td>
                    </tr>
                    <tr height="40px">
                        <th>Email Address</th>
                        <td><input id="txtNewMemberEmail" name="txtNewMemberEmail" type="text" size="50" value="<?=$MemberEmail?>"></td>
                    </tr>
                    <tr height="40px">
                        <th>Password</th>
                        <td><input id="txtNewMemberPassword" name="txtNewMemberPassword" type="password" size="50"></td>
                    </tr>
                    <tr height="40px">
                        <th>Retype Password</th>
                        <td><input id="txtRetypePassword" name="txtRetypePassword" type="password" size="50"></td>
                    </tr>
                    <tr height="40px">
                        <th>Role</th>
                        <td>
                            <select id="txtRole" name="txtRole">
                                <?php
                                include '../includes/dbCon.php';
                                try {
                                    $db = new PDO($dsn, $username, $password, $options);
                                    $sql = $db->prepare("select * from role");
                                    $sql->execute();
                                    $row = $sql->fetch();
                                    while ($row != null){
                                        echo "<option value=<?=$RoleID?>" . $row["RoleValue"] . "</option>>";
                                        $row = $sql->fetch();
                                    }
                                }
                                catch (PDOException $e){
                                    $error = $e->getMessage();
                                    echo "Error: $error";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr height="60px">
                        <td colspan="2">
                            <input type="submit" value="Add New Member" name="submit">
                        </td>
                    </tr>
                </table>
            </form>
            <br /><br />
        </main>
        <footer><?php include '../includes/footer.php'?></footer>
    </body>
</html>