<?php
//  Database Connection Init
include '../includes/dbCon.php';

if(isset($_GET["CustomerID"]))
{
    $id = $_GET["CustomerID"];

    try {


        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("delete from customerlist where CustomerID = :CustomerID");
        $sql->bindValue(":CustomerID", $id);
        $sql->execute();
    }
    catch (PDOException $e){
        $error = $e->getMessage();
        echo "Error: $error";
    }
}
header("Location:customerlist.php");
?>