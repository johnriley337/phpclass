<?php
//  Database Stuff
include '../includes/dbCon.php';

//  Insert Statement
if(isset($_POST["txtFirst"])){
    if(isset($_POST["txtLast"])){
        if(isset($_POST["txtAddress"])){
            if(isset($_POST["txtCity"])){
                if(isset($_POST["txtState"])){
                    if(isset($_POST["txtZip"])){
                        if(isset($_POST["txtPhone"])){
                            if(isset($_POST["txtEmail"])){
                                if(isset($_POST["txtPassword"])){
                                    //  Variables
                                    $firstName = $_POST["txtFirst"];
                                    $lastName = $_POST["txtLast"];
                                    $address = $_POST["txtAddress"];
                                    $city = $_POST["txtCity"];
                                    $state = $_POST["txtState"];
                                    $zip = $_POST["txtZip"];
                                    $phone = $_POST["txtPhone"];
                                    $email = $_POST["txtEmail"];
                                    $inputPassword = $_POST["txtPassword"];
                                    $custid = $_POST["CustomerID"];


                                    try {
                                        $db = new PDO($dsn, $username, $password, $options);
                                        $sql = $db->prepare("update customerlist set FirstName = :FirstName, LastName = :LastName, Address = :Address, City = :City, State = :State, Zip = :Zip, Phone = :Phone, Email = :Email, tblPassword = :Password where CustomerID = :CustomerID");
                                        $sql->bindValue(":FirstName", $firstName);
                                        $sql->bindValue(":LastName", $lastName);
                                        $sql->bindValue(":Address", $address);
                                        $sql->bindValue(":City", $city);
                                        $sql->bindValue(":State", $state);
                                        $sql->bindValue(":Zip", $zip);
                                        $sql->bindValue(":Phone", $phone);
                                        $sql->bindValue(":Email", $email);
                                        $sql->bindValue(":Password", $inputPassword);
                                        $sql->bindValue(":CustomerID", $custid);
                                        $sql->execute();

                                        header("Location:customerlist.php");
                                    }
                                    catch (PDOException $e){
                                        $error = $e->getMessage();
                                        echo "Error: $error";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//  Update Statement
if(isset($_GET["CustomerID"]))
{
    $custid = $_GET["CustomerID"];
    try {
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("select * from customerlist where CustomerID = :CustomerID");
        $sql->bindValue(":CustomerID", $custid);
        $sql->execute();
        $row = $sql->fetch();

        $firstName = $row["FirstName"];
        $lastName = $row["LastName"];
        $address = $row["Address"];
        $city = $row["City"];
        $state = $row["State"];
        $zip = $row["Zip"];
        $phone = $row["Phone"];
        $email = $row["Email"];
        $inputPassword = $row["tblPassword"];
    }
    catch (PDOException $e){
        $error = $e->getMessage();
        echo "Error: $error";
    }
} else {
    echo "ERROR";
    header("Location:customerlist.php");
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer Update</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">

    <!--    Javascript Function -->
    <script type="text/javascript">
        function DeleteCustomer(firstName, custid) {
            if(confirm("Do you want to delete " + firstName)){
                document.location.href = "customerdelete.php?CustomerID=" + custid;
            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <table border="1" width="80%">
            <tr height="60px">
                <td colspan="2"><h3>Update Customer</h3></td>
            </tr>
            <tr height="40px">
                <th>First Name</th>
                <td><input id="txtFirst" name="txtFirst" type="text" size="30" value="<?=$firstName?>"></td>
            </tr>
            <tr height="40px">
                <th>Last Name</th>
                <td><input id="txtLast" name="txtLast" type="text" size="30" value="<?=$lastName?>"></td>
            </tr>
            <tr height="40px">
                <th>Address</th>
                <td><input id="txtAddress" name="txtAddress" type="text" size="50" value="<?=$address?>"></td>
            </tr>
            <tr height="40px">
                <th>City</th>
                <td><input id="txtCity" name="txtCity" type="text" size="30" value="<?=$city?>"></td>
            </tr>
            <tr height="40px">
                <th>State</th>
                <td><input id="txtState" name="txtState" type="text" size="20" value="<?=$state?>"></td>
            </tr>
            <tr height="40px">
                <th>Zip</th>
                <td><input id="txtZip" name="txtZip" type="text" size="30" value="<?=$zip?>"></td>
            </tr>
            <tr height="40px">
                <th>Phone</th>
                <td><input id="txtPhone" name="txtPhone" type="text" size="50" value="<?=$phone?>"></td>
            </tr>
            <tr height="40px">
                <th>Email</th>
                <td><input id="txtEmail" name="txtEmail" type="text" size="80 value="<?=$email?>"></td>
            </tr>
            <tr height="40px">
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="text" size="30" value="<?=$inputPassword?>"></td>
            </tr>
            <tr height="60px">
                <td colspan="2">
                    <input type="submit" value="Update Customer"> | <input type="button" value="Delete Customer" onclick="DeleteCustomer('<?=$firstName?>', '<?=$custid?>')">
                </td>
            </tr>
        </table>
        <input type="hidden" id="CustomerID" name="CustomerID" value="<?=$custid?>">
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
</html>
