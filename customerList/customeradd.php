<?php
if(isset($_POST["txtFirst"])){
    if(isset($_POST["txtLast"])){
        if(isset($_POST["txtAddress"])){
            if(isset($_POST["txtCity"])){
                if(isset($_POST["txtState"])){
                    if(isset($_POST["txtZip"])){
                        if(isset($_POST["txtPhone"])){
                            if(isset($_POST["txtEmail"])){
                                if(isset($_POST["txtPassword"])){
                                    //  Variables
                                    $firstName = $_POST["txtFirst"];
                                    $lastName = $_POST["txtLast"];
                                    $address = $_POST["txtAddress"];
                                    $city = $_POST["txtCity"];
                                    $state = $_POST["txtState"];
                                    $zip = $_POST["txtZip"];
                                    $phone = $_POST["txtPhone"];
                                    $email = $_POST["txtEmail"];
                                    $inputPassword = $_POST["txtPassword"];

                                    //  Database Stuff
                                    include '../includes/dbCon.php';
                                    try {
                                        $db = new PDO($dsn, $username, $password, $options);
                                        $sql = $db->prepare("insert into customerlist (FirstName, LastName, Address, City, State, Zip, Phone, Email, tblPassword) VALUE (:FirstName, :LastName, :Address, :City, :State, :Zip, :Phone, :Email, :Password)");
                                        $sql->bindValue(":FirstName", $firstName);
                                        $sql->bindValue(":LastName", $lastName);
                                        $sql->bindValue(":Address", $address);
                                        $sql->bindValue(":City", $city);
                                        $sql->bindValue(":State", $state);
                                        $sql->bindValue(":Zip", $zip);
                                        $sql->bindValue(":Phone", $phone);
                                        $sql->bindValue(":Email", $email);
                                        $sql->bindValue(":Password", $inputPassword);
                                        $sql->execute();

                                        header("Location:customerlist.php");
                                    }
                                    catch (PDOException $e){
                                        $error = $e->getMessage();
                                        echo "Error: $error";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer Add</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <table border="1" width="80%">
            <tr height="60px">
                <td colspan="2"><h3>Add Customer</h3></td>
            </tr>
            <tr height="40px">
                <th>First Name</th>
                <td><input id="txtFirst" name="txtFirst" type="text" size="30"></td>
            </tr>
            <tr height="40px">
                <th>Last Name</th>
                <td><input id="txtLast" name="txtLast" type="text" size="30"></td>
            </tr>
            <tr height="40px">
                <th>Address</th>
                <td><input id="txtAddress" name="txtAddress" type="text" size="50"></td>
            </tr>
            <tr height="40px">
                <th>City</th>
                <td><input id="txtCity" name="txtCity" type="text" size="30"></td>
            </tr>
            <tr height="40px">
                <th>State</th>
                <td><input id="txtState" name="txtState" type="text" size="20"></td>
            </tr>
            <tr height="40px">
                <th>Zip</th>
                <td><input id="txtZip" name="txtZip" type="text" size="30"></td>
            </tr>
            <tr height="40px">
                <th>Phone</th>
                <td><input id="txtPhone" name="txtPhone" type="text" size="50"></td>
            </tr>
            <tr height="40px">
                <th>Email</th>
                <td><input id="txtEmail" name="txtEmail" type="text" size="80"></td>
            </tr>
            <tr height="40px">
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="text" size="30"></td>
            </tr>
            <tr height="60px">
                <td colspan="2">
                    <input type="submit" value="Add Customer">
                </td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php'?></footer>
</body>
</html>